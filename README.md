# EG-React - Events Viewer test application

This is my response to the EG technical test. 

# Rationales

Probably one of the main controversial things I've done with this app is use a boilerplate, namely
[Max Stoiber's React Boilerplate](https://github.com/mxstbr/react-boilerplate). You do get a lot of
functionality out of the box, provides some great best practices and tooling, and avoids myself 
having to spend hours writing boilerplate code including setting up and configuring tools like 
babel and webpack. I consider this a pragmatic first step to solving the test.

I'm happy to walk through a lot of the main guts of the application, with most of the moving parts
powered by the following:

* React
* Redux
* React-saga
* Reselect

There are some unit tests, although they are not fully comprehensive. The event filter selectors 
probably contain the best tests for inspection.

