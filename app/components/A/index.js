import React, { PropTypes } from 'react';

import styles from './styles.css';

function A(props) {
  return (
    <a
      className={
        props.className || styles.link
      }
      {...props}
    />
  );
}

A.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string.isRequired,
};

export default A;
