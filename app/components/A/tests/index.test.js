import A from '../index';

import expect from 'expect';
import { shallow } from 'enzyme';
import React from 'react';

describe('<A />', () => {
  it('should render its children', () => {
    const children = (<h1>Test</h1>);
    const renderedComponent = shallow(
      <A href="index.html">
        {children}
      </A>
    );
    expect(renderedComponent.contains(children)).toEqual(true);
  });

  it('should adopt the className', () => {
    const renderedComponent = shallow(<A className="test" href="index.html" />);
    expect(renderedComponent.find('a').hasClass('test')).toEqual(true);
  });

  it('should adopt the href', () => {
    const renderedComponent = shallow(<A href="index.html" />);
    expect(renderedComponent.prop('href')).toEqual('index.html');
  });
});
