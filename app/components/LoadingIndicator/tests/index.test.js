import LoadingIndicator from '../index';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import React from 'react';

describe('<LoadingIndicator />', () => {
  it('should have something inside it', () => {
    const renderedComponent = shallow(
      <LoadingIndicator />
    );
    expect(renderedComponent.text()).to.contain('Loading');
  });
});
