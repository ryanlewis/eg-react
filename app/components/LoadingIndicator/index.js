/**
*
* LoadingIndicator
*
*/

import React from 'react';

import styles from './styles.css';

function LoadingIndicator() {
  return (
    <div className={styles.loader}>Loading...</div>
  );
}

export default LoadingIndicator;
