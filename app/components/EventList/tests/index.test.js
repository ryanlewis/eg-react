import { EventList } from '../index';
import configureStore from 'store';

import { expect } from 'chai';
import { render } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';

describe('<EventList />', () => {
  let store = configureStore();

  it('should render the items', () => {
    const items = [
      { title: 'Test 1', image: 'test.gif' },
      { title: 'Test 2', image: 'test.gif' },
    ];

    const renderedComponent = render(
      <Provider store={store}>
        <EventList events={items} />
      </Provider>
    );
    expect(renderedComponent.text()).to.contain('Test 1');
  });
});
