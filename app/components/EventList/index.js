/**
*
* EventList
*
*/

import React from 'react';

import EventListItem from 'containers/EventListItem';

import styles from './styles.css';

export function EventList(props) {
  let content = (<div></div>);

  if (props.events) {
    content = props.events.map((event, index) =>
      (<EventListItem key={`item-${index}`} index={index} event={event} />));
  }

  return (
    <div className={styles.eventList}>
      <ul>
        {content}
      </ul>
    </div>
  );
}

EventList.propTypes = {
  events: React.PropTypes.array.isRequired,
};

export default EventList;
