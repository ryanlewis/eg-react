import React, { PropTypes } from 'react';

import styles from './styles.css';

function Button(props) {
  const className = props.className ? props.className : styles.buttonWrapper;

  return (
    <div className={className}>
      <button onClick={props.handleClick}>{props.children}</button>
    </div>
  );
}

Button.propTypes = {
  className: PropTypes.string,
  handleClick: PropTypes.func,
  children: PropTypes.any,
};

export default Button;
