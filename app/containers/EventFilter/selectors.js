import { createSelector } from 'reselect';

import { selectGlobal } from 'containers/App/selectors';

/**
 * Direct selector to the eventFilter state domain
 */
const selectEventFilterDomain = () => state => state.get('eventFilter');

const selectLocations = () => createSelector(
  selectGlobal(),
  (globalState) => {
    const events = globalState.get('events');
    if (!events) return false;
    let locations = events.map((event) => event.venue);
    // unique values, please
    locations = locations.filter((location, index) => locations.indexOf(location) === index);
    return locations.sort();
  }
);

const selectTypes = () => createSelector(
  selectGlobal(),
  (globalState) => {
    const events = globalState.get('events');
    if (!events) return false;
    let types = events.map((event) => event.type);
    // unique values
    types = types.filter((type, index) => types.indexOf(type) === index);
    return types.sort();
  }
);

const selectFilteredTypes = () => createSelector(
  selectEventFilterDomain(),
  (eventFilterState) => eventFilterState.get('types')
);

const selectFilteredLocations = () => createSelector(
  selectEventFilterDomain(),
  (eventFilterState) => eventFilterState.get('locations')
);

const selectSelectableTypes = () => createSelector(
  selectTypes(),
  (types) => types.map((type) => ({ data: type, selected: false }))
);

export {
  selectEventFilterDomain,
  selectLocations,
  selectTypes,
  selectSelectableTypes,
  selectFilteredTypes,
  selectFilteredLocations,
};
