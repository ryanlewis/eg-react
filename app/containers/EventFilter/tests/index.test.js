import { EventFilter } from '../index';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import React from 'react';

import LoadingIndicator from 'components/LoadingIndicator';

describe('<EventFilter />', () => {
  it('should render a loading indicator when loading', () => {
    const renderedComponent = shallow(
      <EventFilter loading />
    );
    expect(renderedComponent).to.contain(<LoadingIndicator />);
  });
});
