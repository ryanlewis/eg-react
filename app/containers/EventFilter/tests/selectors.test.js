import {
  selectLocations,
  selectTypes,
  selectSelectableTypes,
  selectFilteredTypes,
  selectFilteredLocations,
} from '../selectors';

import { fromJS } from 'immutable';
import { expect } from 'chai';

describe('EventFilter selectors', () => {
  const events = [
    { venue: 'Test 1', type: 'Sporting' },
    { venue: 'Test 2', type: 'Gig' },
    { venue: 'Test 2', type: 'Debate' },
    { venue: 'Test 3', type: 'Sporting' },
  ];

  const typesState = [
    'Sporting', 'Gig',
  ];

  const locationsState = [
    'Test 1', 'Test 2',
  ];

  const globalState = fromJS({ global: {}, eventFilter: {} });

  const mockedState = globalState
    .setIn(['global', 'events'], events)
    .setIn(['eventFilter', 'types'], typesState)
    .setIn(['eventFilter', 'locations'], locationsState);

  describe('selectLocations', () => {
    const locationSelector = selectLocations();

    it('should return a list of locations', () => {
      const locations = locationSelector(mockedState);
      expect(locations).to.contain('Test 1');
    });

    it('should return a list of unique locations', () => {
      const locations = locationSelector(mockedState);
      expect(locations).to.have.length(3);
    });
  });

  describe('selectTypes', () => {
    const typeSelector = selectTypes();

    it('should return a list of types', () => {
      const types = typeSelector(mockedState);
      expect(types).to.contain('Sporting');
    });

    it('should return a list of unique types', () => {
      const types = typeSelector(mockedState);
      expect(types).to.have.length(3);
    });
  });

  describe('selectSelectableTypes', () => {
    const selectableTypeSelector = selectSelectableTypes();

    it('should return a list of objects', () => {
      const selectableTypes = selectableTypeSelector(mockedState);
      const first = selectableTypes.shift();
      expect(first).to.be.an('object');
    });
  });

  describe('selectFilteredTypes', () => {
    const filteredTypeSelector = selectFilteredTypes();

    it('should return a list of filtered types', () => {
      const filteredTypes = filteredTypeSelector(mockedState);
      expect(filteredTypes).to.have.length(2);
    });
  });

  describe('selectFilteredLocations', () => {
    const filteredLocationSelector = selectFilteredLocations();
    it('should return a list of filtered locations', () => {
      const filteredTypes = filteredLocationSelector(mockedState);
      expect(filteredTypes).to.have.length(2);
    });
  });
});
