/*
 *
 * EventFilter actions
 *
 */

import {
  CHANGE_SELECTED_TYPES,
  CHANGE_SELECTED_LOCATIONS,
  CHANGE_SELECTED_KEYWORD,
} from './constants';

export function changeSelectedTypes(types) {
  return {
    type: CHANGE_SELECTED_TYPES,
    types,
  };
}

export function changeSelectedLocations(locations) {
  return {
    type: CHANGE_SELECTED_LOCATIONS,
    locations,
  };
}

export function changeSelectedKeyword(keyword) {
  return {
    type: CHANGE_SELECTED_KEYWORD,
    keyword,
  };
}
