/*
 *
 * EventFilter constants
 *
 */

export const CHANGE_SELECTED_TYPES = 'app/EventFilter/CHANGE_SELECTED_TYPES';
export const CHANGE_SELECTED_LOCATIONS = 'app/EventFilter/CHANGE_SELECTED_LOCATIONS';
export const CHANGE_SELECTED_KEYWORD = 'app/EventFilter/CHANGE_SELECTED_KEYWORD';
