/*
 *
 * EventFilter
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import LoadingIndicator from 'components/LoadingIndicator';
import CheckBoxList from 'containers/CheckBoxList';

import { selectLoading } from 'containers/App/selectors';
import { selectLocations, selectTypes } from './selectors';
import { changeSelectedTypes, changeSelectedLocations, changeSelectedKeyword } from './actions';

import styles from './styles.css';

export class EventFilter extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    let content;

    if (this.props.loading) {
      content = (<LoadingIndicator />);
    } else {
      content = (
        <div>

          <div className={styles.heading}>Event Locations</div>
          <fieldset className={styles.fieldset}>
            <CheckBoxList items={this.props.locations} onChange={this.props.onChangeLocation} />
          </fieldset>

          <div className={styles.heading}>Event Types</div>
          <fieldset className={styles.fieldset}>
            <CheckBoxList items={this.props.types} onChange={this.props.onChangeType} />
          </fieldset>

          <div>
            <div className={styles.heading}>Keywords</div>
            <input className={styles.input} type="text" placeholder="Search..." onChange={this.props.onChangeKeyword} />
          </div>
        </div>);
    }

    return (
      <div className={styles.eventFilter}>
        {content}
      </div>
    );
  }
}

EventFilter.propTypes = {
  loading: React.PropTypes.bool,
  locations: React.PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.bool,
  ]),
  types: React.PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.bool,
  ]),
  onChangeType: React.PropTypes.func,
  onChangeLocation: React.PropTypes.func,
  onChangeKeyword: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  loading: selectLoading(),
  locations: selectLocations(),
  types: selectTypes(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeType: (types) => dispatch(changeSelectedTypes(types)),
    onChangeLocation: (locations) => dispatch(changeSelectedLocations(locations)),
    onChangeKeyword: (evt) => dispatch(changeSelectedKeyword(evt.target.value)),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EventFilter);
