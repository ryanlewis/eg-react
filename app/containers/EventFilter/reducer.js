/*
 *
 * EventFilter reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_SELECTED_TYPES,
  CHANGE_SELECTED_LOCATIONS,
  CHANGE_SELECTED_KEYWORD,
} from './constants';

const initialState = fromJS({
  types: false,
  locations: false,
  keyword: '',
});

function eventFilterReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_SELECTED_TYPES:
      return state
        .set('types', action.types);
    case CHANGE_SELECTED_LOCATIONS:
      return state
        .set('locations', action.locations);
    case CHANGE_SELECTED_KEYWORD:
      return state
        .set('keyword', action.keyword);
    default:
      return state;
  }
}

export default eventFilterReducer;
