/**
 * Test the HomePage
 */

import { expect } from 'chai';
import { shallow } from 'enzyme';
import React from 'react';

import { HomePage } from '../index';
import EventList from 'components/EventList';
import LoadingIndicator from 'components/LoadingIndicator';

describe('<HomePage />', () => {
  it('should render the loading indicator when its loading', () => {
    const renderedComponent = shallow(
      <HomePage loading />
    );
    expect(renderedComponent).to.contain(<LoadingIndicator />);
  });

  it('should render the events if loading was successful', () => {
    const events = [{
      id: '238975',
      title: 'Junkyard Golf Club at B.EAT STREET, Manchester',
      date: 'Tuesday 5th April',
      image: 'http://images/events/1456917646.jpg',
      type: 'Sporting',
      venue: 'Beat Street MCR, Manchester',
    }];

    const renderedComponent = shallow(
      <HomePage
        events={events}
        error={false}
        currentEvent={false}
      />
    );

    expect(renderedComponent).to.contain(<EventList events={events} />);
  });
});
