/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a neccessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Helmet from 'react-helmet';

import { selectFilteredEvents, selectError, selectLoading, selectCurrentEvent } from 'containers/App/selectors';
import { loadEvents } from 'containers/App/actions';

import LoadingIndicator from 'components/LoadingIndicator';
import EventList from 'components/EventList';
import EventFilter from 'containers/EventFilter';

import styles from './styles.css';

export class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    if (this.props.events === false) {
      this.props.loadEvents();
    }
  }

  render() {
    let mainContent = null;

    if (this.props.loading) {
      return (<div className={styles.loadingWrapper}><LoadingIndicator /></div>);
    } else if (this.props.error !== false) {
      mainContent = (<div>Something went wrong.. :(</div>);
    } else if (this.props.events !== false && this.props.currentEvent === false) {
      mainContent = (<EventList events={this.props.events} />);
    }

    return (
      <div className={styles.homeWrapper}>
        <Helmet
          title="Event Lister"
          meta={[
            { name: 'description', content: 'A neat listing of some events, made with React' },
          ]}
        />

        <aside className={styles.filters}>
          <EventFilter />
        </aside>

        <section className={styles.listing}>
          {mainContent}
        </section>
      </div>
    );
  }
}

HomePage.propTypes = {
  loading: React.PropTypes.bool,
  events: React.PropTypes.oneOfType([React.PropTypes.array, React.PropTypes.bool]),
  error: React.PropTypes.bool,
  currentEvent: React.PropTypes.oneOfType([React.PropTypes.object, React.PropTypes.bool]),
  loadEvents: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  loading: selectLoading(),
  events: selectFilteredEvents(),
  error: selectError(),
  currentEvent: selectCurrentEvent(),
}); // something to do with this, why the selectFilteredEvents isnt working?

function mapDispatchToProps(dispatch) {
  return {
    loadEvents: () => dispatch(loadEvents()),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
