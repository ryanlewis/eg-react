import {
  LOAD_EVENTS,
  LOAD_EVENTS_SUCCESS,
  LOAD_EVENTS_ERROR,
  SELECT_EVENT,
//  LOAD_EVENT,
  LOAD_EVENT_SUCCESS,
  LOAD_EVENT_ERROR,

  DESELECT_EVENT,
} from './constants';

export function loadEvents() {
  return {
    type: LOAD_EVENTS,
  };
}

export function eventsLoaded(events) {
  return {
    type: LOAD_EVENTS_SUCCESS,
    events,
  };
}

export function eventsLoadingError(error) {
  return {
    type: LOAD_EVENTS_ERROR,
    error,
  };
}

export function selectEvent(id) {
  return {
    type: SELECT_EVENT,
    eventId: id,
  };
}

export function eventLoaded(event) {
  return {
    type: LOAD_EVENT_SUCCESS,
    event,
  };
}

export function eventLoadingError(error) {
  return {
    type: LOAD_EVENT_ERROR,
    error,
  };
}

export function deselectEvent() {
  return {
    type: DESELECT_EVENT,
  };
}
