export const LOAD_EVENTS = 'eg-react/App/LOAD_EVENTS';
export const LOAD_EVENTS_SUCCESS = 'eg-react/App/LOAD_EVENTS_SUCCESS';
export const LOAD_EVENTS_ERROR = 'eg-react/App/LOAD_EVENTS_ERROR';

export const SELECT_EVENT = 'eg-react/App/SELECT_EVENT';

export const LOAD_EVENT = 'eg-react/App/LOAD_EVENT';
export const LOAD_EVENT_SUCCESS = 'eg-react/App/LOAD_EVENT_SUCCESS';
export const LOAD_EVENT_ERROR = 'eg-react/App/LOAD_EVENT_ERROR';

export const DESELECT_EVENT = 'eg-react/App/DESELECT_EVENT';
