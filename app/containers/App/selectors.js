import { createSelector } from 'reselect';

// selectLocationState expects a plain JS object for the routing state
const selectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

const selectGlobal = () => (state) => state.get('global');

const selectLoading = () => createSelector(
  selectGlobal(),
  (globalState) => globalState.get('loading')
);

const selectEvents = () => createSelector(
  selectGlobal(),
  (globalState) => globalState.get('events')
);

const selectError = () => createSelector(
  selectGlobal(),
  (globalState) => globalState.get('error')
);

const selectCurrentEvent = () => createSelector(
  selectGlobal(),
  (globalState) => globalState.get('currentEvent')
);

const getFilteredTypes = () => (state) => state.getIn(['eventFilter', 'types']);
const getFilteredLocations = () => (state) => state.getIn(['eventFilter', 'locations']);
const getKeyword = () => (state) => state.getIn(['eventFilter', 'keyword']);

const selectFilteredEvents = () => createSelector(
  [selectEvents(), getFilteredTypes(), getFilteredLocations(), getKeyword()],
  (events, types, locations, keyword) => {
    if (events === false || types === false || locations === false) return false;
    let filtered = events
      .filter((event) => types.indexOf(event.type) >= 0)
      .filter((event) => locations.indexOf(event.venue) >= 0);

    if (typeof(keyword) === 'string' && keyword.length > 2) {
      filtered = filtered.filter((event) => event.title.toLowerCase().indexOf(keyword.toLowerCase()) > -1);
    }

    return filtered;
  }
);

export {
  selectGlobal,
  selectLocationState,
  selectEvents,
  selectError,
  selectLoading,
  selectCurrentEvent,
  selectFilteredEvents,
};
