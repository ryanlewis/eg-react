import 'whatwg-fetch';
import { takeLatest } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import { eventsLoaded, eventsLoadingError, eventLoaded, eventLoadingError } from './actions';
import { LOAD_EVENTS, SELECT_EVENT, LOAD_EVENT_SUCCESS, DESELECT_EVENT } from './constants';

const doFetch = (url) => fetch(url)
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return response;
      }

      const error = new Error(response.statusText);
      error.response = response;
      throw error;
    })
    .then((response) => response.json())
    .catch((err) => err);

// fired on LOAD_EVENTS
function* fetchEvents() {
  const eventsUrl = 'http://tech-test.egtools.co.uk/events/';
  let events = yield call(doFetch, eventsUrl);

  // TODO: at this point, I'd sort the events by date, if we had a sane date property (e.g. no Year)

  if (!events.err) {
    // artifically cap the number of events to 100 - we're just demonstrating here, some form of paging
    // from the server makes a lot of sense here
    events = events.slice(0, 100);

    yield put(eventsLoaded(events));
  } else {
    yield put(eventsLoadingError(events.err));
  }
}

// fired on SELECT_EVENT
function* fetchEvent(action) {
  const eventUrl = `http://tech-test.egtools.co.uk/events/${action.eventId}`;
  const event = yield call(doFetch, eventUrl);

  if (!event.err) {
    yield put(eventLoaded(event));
  } else {
    yield put(eventLoadingError(event.err));
  }
}

// fired once an event is loaded with LOAD_EVENT_SUCCESS
function* reRouteToEventDetails(action) {
  const route = `/details/${action.event.id}`;
  yield put(push(route));
}

function* reRouteToHomePage() {
  yield put(push('/'));
}

// Our main watcher
function* eventsSaga() {
  yield [
    takeLatest(LOAD_EVENTS, fetchEvents),
    takeLatest(SELECT_EVENT, fetchEvent),
    takeLatest(LOAD_EVENT_SUCCESS, reRouteToEventDetails),
    takeLatest(DESELECT_EVENT, reRouteToHomePage),
  ];
}

export default eventsSaga;
