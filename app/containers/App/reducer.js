import { fromJS } from 'immutable';

import {
  LOAD_EVENTS,
  LOAD_EVENTS_SUCCESS,
  LOAD_EVENTS_ERROR,
  SELECT_EVENT,
  // LOAD_EVENT,
  LOAD_EVENT_SUCCESS,
  LOAD_EVENT_ERROR,
  DESELECT_EVENT,
} from './constants';

import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = fromJS({
  loading: false,
  error: false,
  events: false,
  currentEvent: false,
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_EVENTS:
      return state
        .set('loading', true)
        .set('error', false)
        .set('events', false)
        .set('currentEvent', false);

    case LOAD_EVENTS_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('events', action.events);

    case LOAD_EVENTS_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error);

    case SELECT_EVENT:
      return state
        .set('loading', true);

    case LOAD_EVENT_SUCCESS:
      return state
        // .set('loading', false)
        .set('error', false)
        .set('currentEvent', action.event);

    case LOAD_EVENT_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error);

    case DESELECT_EVENT:
      return state
        .set('loading', true)
        .set('currentEvent', false);

    case LOCATION_CHANGE:
      return state
        .set('loading', false);

    default:
      return state;
  }
}

export default appReducer;
