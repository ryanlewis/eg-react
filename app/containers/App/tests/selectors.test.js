import { fromJS } from 'immutable';
import { expect } from 'chai';

import { selectLocationState, selectFilteredEvents } from 'containers/App/selectors';

describe('selectLocationState', () => {
  it('should select the route as a plain JS object', () => {
    const route = fromJS({
      locationBeforeTransitions: null,
    });
    const mockedState = fromJS({
      route,
    });
    expect(selectLocationState()(mockedState)).to.eql(route.toJS());
  });
});

describe('selectFilteredEvents', () => {
  const events = [
    { venue: 'Test 1', type: 'Sporting' },
    { venue: 'Test 2', type: 'Gig' },
    { venue: 'Test 2', type: 'Debate' },
    { venue: 'Test 3', type: 'Sporting' },
  ];

  const typesState = ['Sporting', 'Gig'];
  const locationsState = ['Test 1'];
  const globalState = fromJS({ global: {}, eventFilter: {} });
  const mockedState = globalState
    .setIn(['global', 'events'], events)
    .setIn(['eventFilter', 'types'], typesState)
    .setIn(['eventFilter', 'locations'], locationsState);

  const filteredEventsSelector = selectFilteredEvents();
  it('should select only one event with filters applied', () => {
    const filteredEvents = filteredEventsSelector(mockedState);
    expect(filteredEvents).to.have.length(1);
  });
});
