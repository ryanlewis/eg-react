import { EventListItem } from '../index';

import expect from 'expect';
import { mount } from 'enzyme';
import React from 'react';

describe('<EventListItem />', () => {
  let event;

  beforeEach(() => {
    event = {
      id: '238975',
      title: 'Junkyard Golf Club at B.EAT STREET, Manchester',
      date: 'Tuesday 5th April',
      image: 'http://images/events/1456917646.jpg',
      type: 'Sporting',
      venue: 'Beat Street MCR, Manchester',
    };
  });

  it('should render the event title', () => {
    const renderedComponent = mount(
      <EventListItem event={event} />
    );
    expect(renderedComponent.text().indexOf(event.title)).toBeGreaterThan(-1);
  });
});
