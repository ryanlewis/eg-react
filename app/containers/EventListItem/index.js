/*
 *
 * EventListItem
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { selectEvent } from 'containers/App/actions';

import Img from 'components/Img';

import styles from './styles.css';

export class EventListItem extends React.Component { // eslint-disable-line react/prefer-stateless-function

  selectEvent = () => this.props.selectEvent(this.props.event.id);

  cleanString = (str) => {
    if (typeof(str) !== 'string') return str;
    return str.replace(/&amp;/g, '&').replace(/&#039;/g, '\'');
  }

  render() {
    const eventTitle = this.cleanString(this.props.event.title);
    const venue = this.cleanString(this.props.event.venue);

    return (
      <div className={styles.eventListItem} onClick={this.selectEvent}>
        <Img className={styles.image} src={this.props.event.image} alt={eventTitle} />
        <div className={styles.innerWrap}>
          <div className={styles.title}>{eventTitle}</div>
          <div className={styles.date}>{this.props.event.date}</div>
          <div className={styles.venue}>{venue}</div>
        </div>
      </div>
    );
  }
}

EventListItem.propTypes = {
  selectEvent: React.PropTypes.func,
  event: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
};

function mapDispatchToProps(dispatch) {
  return {
    changeRoute: (url) => dispatch(push(url)),
    selectEvent: (eventId) => dispatch(selectEvent(eventId)),
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(EventListItem);
