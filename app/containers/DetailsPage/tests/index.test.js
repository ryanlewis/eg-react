import { DetailsPage } from '../index';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import React from 'react';

describe('<DetailsPage />', () => {
  it('should render the event if currentEvent is set', () => {
    const event = {
      id: '238975',
      title: 'Junkyard Golf Club at B.EAT STREET, Manchester',
      url: 'http://www.ticketarena.co.uk/events/EVENT/index.html',
      date: 'Tuesday 5th April',
      unixdate: 1459861200,
      venue: 'Beat Street MCR, Manchester',
      image: 'http://images/events/1456917646.jpg',
      type: 'Sporting',
      information_title: 'Junkyard Golf Club at B.EAT STREET',
      information_description: 'Lorem Ipsum',
    };

    const renderedComponent = shallow(
      <DetailsPage event={event} />
    );
    expect(renderedComponent.text()).to.contain('Junkyard Golf Club');
  });
});
