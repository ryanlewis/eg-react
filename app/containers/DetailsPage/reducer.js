/*
 *
 * DetailsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DESELECT_EVENT,
} from './constants';

const initialState = fromJS({});

function detailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case DESELECT_EVENT:
      return state;
    default:
      return state;
  }
}

export default detailsPageReducer;
