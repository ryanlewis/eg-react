/*
 *
 * DetailsPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';

import { selectCurrentEvent } from 'containers/App/selectors';
import { selectEvent, deselectEvent } from 'containers/App/actions';

import Img from 'components/Img';
import Button from 'components/Button';

import styles from './styles.css';

export class DetailsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    // check to see if event is false, if it is, dispatch a select event to grab the event details
    if (!this.props.currentEvent) {
      this.props.selectEvent(this.props.params.eventId);
    }
  }

  deselectEvent = () => {
    this.props.deselectEvent(this.props.event);
  }

  render() {
    let title;
    let description;

    if (this.props.event !== false) {
      title = this.props.event.information_title;
      description = `Details of ${this.props.event.title}`;
    }

    return (
      <div className={styles.detailsPage}>
        <Helmet
          title={title}
          meta={[
            { name: 'description', content: description },
          ]}
        />
        <Img className={styles.image} src={this.props.event.image} alt={title} />
        <div className={styles.date}>{this.props.event.date}</div>
        <div className={styles.title}>{title}</div>
        <div className={styles.type}>Event Type: {this.props.event.type}</div>
        <div><a href={this.props.event.url}>More Information</a></div>

        <Button handleClick={this.deselectEvent}>Back to Listing</Button>
      </div>
    );
  }
}

DetailsPage.propTypes = {
  event: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  currentEvent: React.PropTypes.any,
  deselectEvent: React.PropTypes.func,
  selectEvent: React.PropTypes.func,
  params: React.PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  event: selectCurrentEvent(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    selectEvent: (id) => dispatch(selectEvent(id)),
    deselectEvent: (event) => dispatch(deselectEvent(event)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);
