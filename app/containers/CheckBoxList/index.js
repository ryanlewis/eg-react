/*
 *
 * CheckBoxList
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.css';

export class CheckBoxList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { checked: [] };
  }

  componentWillMount() {
    if (this.state.checked.length === 0 && this.props.items) {
      const checked = this.props.items.map((item) =>
        ({ value: item, checked: true }));

      this.setState({ checked }, () => this.doOnChange());
    }
  }

  handleCheck = (event) => {
    const target = event.target;
    const checked = target.checked;
    const value = target.value;

    // state is an object with the value and the checked status
    const checkedStatus = this.state.checked.find((s) => s.value === value);

    // update that particular state
    checkedStatus.checked = checked;

    // update state again
    this.setState({ checked: this.state.checked }, () => this.doOnChange());
  }

  // returns array of strings that are checked
  selectedValues = () => this.state.checked.filter((item) => item.checked === true).map((item => item.value));

  // call onChange with our selected values
  doOnChange = () => this.props.onChange(this.selectedValues());

  render() {
    let checkBoxes;
    if (this.props.items && this.state.checked.length) {
      checkBoxes = this.props.items.map((item, index) => {
        const state = this.state.checked.find((checkedState) => checkedState.value === item);
        const isChecked = state !== undefined && state.checked;

        return (<label className={styles.label} key={`item-${index}`}><input type="checkbox" value={item} onChange={this.handleCheck} checked={isChecked} /> {item}</label>);
      });
    }

    return (
      <div className={styles.checkBoxList}>
        {checkBoxes}
      </div>
    );
  }
}

CheckBoxList.propTypes = {
  items: React.PropTypes.oneOfType([React.PropTypes.array, React.PropTypes.bool]),
  checkBoxes: React.PropTypes.array,
  onChange: React.PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(CheckBoxList);
